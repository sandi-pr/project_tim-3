<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//route profile
Route::resource('profile', 'App\Http\Controllers\ProfileController');
//route kategori
Route::resource('kategori', 'App\Http\Controllers\KategoriController');
//route tanya
Route::resource('tanya', 'App\Http\Controllers\TanyaController');
//route jawaban
Route::resource('jawab', 'App\Http\Controllers\JawabController');




Auth::routes();
