<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
    protected $table = "jawab";
    protected $fillable = ["jawaban", "tanya_id", "user_id"];

    public function tanya(){
        return $this->belongsTo('App\Tanya', 'tanya_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
