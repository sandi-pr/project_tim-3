<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanya extends Model
{
    protected $table = "tanya";
    protected $fillable = ["pertanyaan", "gambar", "kategori_id", "user_id"];

    public function kategori(){
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function jawab()
    {
    return $this->hasMany('App\Jawab');
    }
}
